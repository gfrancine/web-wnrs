// https://github.com/mdn/dom-examples/blob/main/screen-wake-lock-api/script.js

if ("wakeLock" in navigator) {
  let wakeLock = null;

  const requestWakeLock = async () => {
    try {
      wakeLock = await navigator.wakeLock.request("screen");
      wakeLock.addEventListener("release", (ev) => {
        console.log(ev);
      });
    } catch (err) {
      console.error(err);
    }
  };

  requestWakeLock();

  document.addEventListener("visibilitychange", () => {
    if (wakeLock !== null && document.visibilityState === "visible") {
      requestWakeLock();
    }
  });
}
