export type Deck = {
  final: string;
  level1: string[];
  level2: string[];
  level3: string[];
  wildcard: string[];
};

export type LevelID = "level1" | "level2" | "level3" | "final";
