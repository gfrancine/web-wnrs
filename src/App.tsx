import React, { useEffect, useState } from "react";
import { Help } from "./Help";
import { Deck, LevelID } from "./types";

function Cover() {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div
      className={"cover" + (isOpen ? " open" : "")}
      onClick={() => setIsOpen(true)}
    >
      <p className="game-title">WE'RE NOT REALLY STRANGERS</p>
      <br />
      <p>
        WARNING: <br /> FEELINGS MAY ARISE
      </p>
    </div>
  );
}

function Level(props: { name: string; cards: string[]; visible: boolean }) {
  const [index, setIndex] = useState(0);
  const [isTransitioning, setIsTransitioning] = useState(false);

  const next = () => {
    if (index === props.cards.length - 1) return;
    if (isTransitioning) return;

    setIsTransitioning(true);
    setTimeout(() => {
      setIsTransitioning(false);
      setIndex(index + 1);
    }, 300);
  };

  const back = () => {
    if (index === 0) return;
    if (isTransitioning) return;

    setIsTransitioning(true);
    setTimeout(() => {
      setIsTransitioning(false);
      setIndex(index - 1);
    }, 300);
  };

  return (
    <div className={"level" + (props.visible ? "" : " hidden")}>
      <div className="card-container">
        {/*<div className="back-card"></div>*/}
        <div className={"card" + (isTransitioning ? " offscreen" : "")}>
          <p className="title">{props.name}</p>
          <h2>{props.cards[index]}</h2>
          <p>
            <small>We're not really strangers</small>
          </p>
        </div>
      </div>
      <div className="nav">
        <button className="button" disabled={index === 0} onClick={back}>
          <span>⏴</span>
        </button>
        <span>
          {index + 1}/{props.cards.length}
        </span>
        <button
          className="button"
          disabled={index === props.cards.length - 1}
          onClick={next}
        >
          <span>⏵</span>
        </button>
      </div>
    </div>
  );
}

export function App(props: { deck: Deck }) {
  const [helpVisible, setHelpVisible] = useState(true);
  const [deck, _setDeck] = useState(props.deck);
  const [level, setLevel] = useState<LevelID>("level1");

  const makeLevelSelectButton = (levelID: LevelID, levelName) => (
    <li>
      <button
        className="button"
        disabled={level === levelID}
        onClick={() => setLevel(levelID)}
      >
        {levelName}
      </button>
    </li>
  );

  return (
    <div className="app">
      <div className="topbar">
        <ul className="level-select">
          {makeLevelSelectButton("level1", "L1")}
          {makeLevelSelectButton("level2", "L2")}
          {makeLevelSelectButton("level3", "L3")}
          {makeLevelSelectButton("final", "F")}
        </ul>
        <ul>
          <li>
            <button className="button" onClick={() => setHelpVisible(true)}>
              ?
            </button>
          </li>
        </ul>
      </div>
      <Level
        name="Level 1 (Perception)"
        cards={deck.level1}
        visible={level === "level1"}
      />
      <Level
        name="Level 2 (Connection)"
        cards={deck.level2}
        visible={level === "level2"}
      />
      <Level
        name="Level 3 (Reflection)"
        cards={deck.level3}
        visible={level === "level3"}
      />
      <Level
        name="Final Card"
        cards={[deck.final]}
        visible={level === "final"}
      />
      {helpVisible ? (
        <div className="overlay">
          <div className="modal">
            <button
              className="button close"
              onClick={() => setHelpVisible(false)}
            >
              <span>X</span>
            </button>
            <Help />
            <p style={{ textAlign: "center", margin: "2em" }}>
              <button
                className="text-button"
                onClick={() => setHelpVisible(false)}
              >
                Start Playing
              </button>
            </p>
          </div>
        </div>
      ) : (
        <></>
      )}
      <Cover />
    </div>
  );
}
