import "./index.scss";
import React from "react";
import { render } from "react-dom";
import { App } from "./App";
import Papa from "papaparse";
import { Deck } from "./types";

const root = document.querySelector("#root");

let deck: Deck = {
  final: "",
  level1: [],
  level2: [],
  level3: [],
  wildcard: [],
};

// https://stackoverflow.com/a/12646864
function shuffleArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }
}

fetch("wnrs.csv")
  .then((res) => res.text())
  .then((str) => {
    const data = Papa.parse(str).data.filter((row) => row[0] && row[1]);

    for (const row of data) {
      if (row[0] === "FINAL CARD") {
        deck.final = row[1];
      } else if (row[0] === "lvl1 -") {
        deck.level1.push(row[1]);
      } else if (row[0] === "lvl2 -") {
        deck.level2.push(row[1]);
      } else if (row[0] === "lvl3 -") {
        deck.level3.push(row[1]);
      } else if (row[0] === "WILDCARD") {
        deck.wildcard.push(row[1]);
      }
    }

    [deck.level1, deck.level2, deck.level3].forEach(shuffleArray);
  })
  .then(() => {
    render(<App deck={deck} />, root);
  });
