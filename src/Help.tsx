import React from "react";

export function Help() {
  return (
    <>
      <h1>WE'RE NOT REALLY STRANGERS</h1>
      <p>
        <a href="https://www.werenotreallystrangers.com/">
          We're Not Really Strangers
        </a>{" "}
        is a popular icebreaking card game for deepening existing relationships
        and creating new ones.{" "}
      </p>
      <p>The cards are divided into three levels. </p>

      <ul>
        <li>
          <p>
            {" "}
            Level 1 cards are known as “Perception” cards, and are all about
            your impressions of yourself and the other players.
          </p>
        </li>
        <li>
          <p>
            Level 2 cards are called “Connection” cards, and take a deeper
            emotional dive into each player's past.
          </p>
        </li>
        <li>
          <p>
            Level 3 cards are known as “Reflection” cards, and give you a chance
            to reflect on the connections you've made with the other players.
          </p>
        </li>
        <li>
          <p>The game will end with the final card.</p>
        </li>
      </ul>

      <h2>HOW TO PLAY</h2>
      <ol>
        <li>
          <p>Players will take turns to draw cards. Decide who goes first.</p>
        </li>
        <li>
          <p>
            The player will read a Level 1 card and invite all the other players
            to answer.
          </p>
        </li>
        <li>
          <p>
            Every player has a right to tell another player to "DIG DEEPER" and
            have them elaborate their answer more.
          </p>
        </li>
        <li>
          <p>
            Pass the turn to the next player, and repeat until each player has
            drawn at least 2 cards (depending on the group size—up to 15 cards
            are usually drawn for two players).
          </p>
        </li>
        <li>
          <p>Advance to the next level and repeat the process.</p>
        </li>
        <li>
          <p>Finish the game with the final card activity.</p>
        </li>
      </ol>

      <h2>HOW TO USE WEBWNRS</h2>
      <ul>
        <li>
          <p>The levels can be selected from the topbar.</p>
        </li>
        <li>
          <p>
            Press the next and back buttons on the bottom to go through the
            cards in the level. Every level has already been shuffled.
          </p>
        </li>
        <li>
          <p>You can reshuffle the cards by refreshing the page.</p>
        </li>
      </ul>
    </>
  );
}
